const { default: axios } = require("axios");
const TelegramBot = require("node-telegram-bot-api");

// replace the value below with the Telegram token you receive from @BotFather
const token = "#";

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, { polling: true });

// Matches "/echo [whatever]"
bot.onText(/\/rate/, (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
  const chatId = msg.chat.id;
  //   const resp = match[1]; // the captured "whatever"

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, "What currency are you interested in?", {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: "€ - EUR",
            callback_data: "EUR",
          },
          {
            text: "$ - USD",
            callback_data: "USD",
          },
          {
            text: "₿ - BTC",
            callback_data: "BTC",
          },
        ],
      ],
    },
  });
});

// Listen for any kind of message. There are different kinds of
// messages.
/* bot.on("message", (msg) => {
  const chatId = msg.chat.id;

  // send a message to the chat acknowledging receipt of their message
  bot.sendMessage(chatId, "Received your message");
});
 */

bot.on("callback_query", (query) => {
  const id = query.message.chat.id;

  const flag = {
    EUR: "💶",
    USD: "💵",
    BTC: "🪙",
    UAH: "🇺🇦",
  };

  axios
    .get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")
    .then((response) => {
      const data = response.data;
      const result = data.filter((item) => item.ccy === query.data)[0];
      return `
      *${flag[result.ccy]}${result.ccy} 💱 ${result.base_ccy}${
        flag[result.base_ccy]
      }*
      Buy: ${result.buy}
      Sale: ${result.sale}
      `;
    })
    .then((msg) => {
      bot.sendMessage(id, msg, { parse_mode: "Markdown" });
    })
    .catch((err) => console.log(err));
});
